use std::{
    fs,
    sync::{
        Arc,
        Mutex,
        atomic::{
            AtomicBool,
            Ordering
        }
    }
};

use rand::{
    thread_rng,
    Rng
};

use serenity::{
    model::user::User
};

lazy_static! {
    static ref COMPLIMENTS: Arc<Mutex<Vec<String>>> = Arc::new(Mutex::new(Vec::new()));
    //static ref LOADED: Arc<Mutex<bool>> = Arc::new(Mutex::new(false));
    static ref LOADED: AtomicBool = AtomicBool::new(false);
}

pub fn load_compliments(file_name: &str) {
    //let mut compliments: Vec<String> = Vec::new();
    if LOADED.compare_and_swap(false, true, Ordering::Relaxed) {
        println!("[ ERROR ] Compliments are already loaded, \"load_compliments()\" sould not be multiple times");
    } else {
        let file = fs::read_to_string(file_name).unwrap();
        for compliment in file.split('\n') {
            COMPLIMENTS.lock().unwrap().push(String::from(compliment));
        }
    }
}

pub fn get_compliment(user: &User) -> String {
    let len = COMPLIMENTS.lock().unwrap().len();
    let compliment_string = &COMPLIMENTS.lock().unwrap()[thread_rng().gen_range(0,len)];
    compliment_string.replace("{}", &user.name)
}
