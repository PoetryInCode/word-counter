use std::{
    fs::File,
    thread,
	sync::{
		Arc,
		Mutex
	},
	io::{
	    BufReader,
	    BufRead
	}
};

lazy_static! {
    static ref INDEX: Arc<Mutex<Vec<(String, String)>>> = Arc::new(Mutex::new(Vec::new()));
}

pub fn init() {
    let fname = "./ssif";
    let f = match File::open(fname) {
        Ok(o) => o,
        Err(e) => panic!("[ERROR] Unable to read file \"{}\": {}", fname, e)
    };
    let reader = BufReader::new(f);

    let mut term = String::new();
    let mut desc = String::new();

    for line in reader.lines() {
        let line: String = line.unwrap();

        if line.starts_with(":") {
            if !desc.is_empty() {
                INDEX.lock().unwrap().push((term, String::from(&desc)));
                desc = String::new();
            }
            term = String::from(&line[1..]);
        } else {
            desc.push_str(&line);
        }
    }
}

pub fn ssif<'a>(lang: String) -> Result<String, String> {
    let index = INDEX.lock().unwrap().clone();
    let lang = lang.to_uppercase();
    for entry in index.iter() {
        let a = entry.0.to_uppercase();
        if a == lang {
            return Ok(format!("{}:\n{}", entry.0, entry.1));
        }
    }
    Err(String::from(format!("No language \"{}\" in page", lang)))
}
