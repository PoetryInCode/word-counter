pub fn get_page(topic: String) -> Option<String> {
    let topic = topic.to_uppercase();
    let a = || -> &str {
        match topic.as_str() {
            "HELP" | "TOPICS" => "The available help topics are:`help` `ssif` `wordcount` `compliment`",
            "COMPLIMENT" => "`:compliment &optional mention`\nWill give you, or someone else a nice 'compliment'",
            "WORDCOUNT" => "`:wordcount word`\nWill tell you the number of times that word has been used in the server",
            "SSIF" => "`:ssif lang`\n Will tell you how you **S**hoot your**S**elf **I**n the **F**oot with your favorite programming language",
            _ => "Please use of of the avaiable topics do `:help topics` to view them"
        }
    };
    Some(String::from(a()))
}
