#![feature(async_closure)]

use std::path::Path;

use std::{
	fs,
	io::{
		Write,
		stdout,
		stdin,
		ErrorKind
	},
	string::String,
	thread,
	sync::{
		Arc,
		Mutex
	}
};

use serenity::{
	async_trait,
	client::{
		Client,
		Context,
		EventHandler
	},
	model::{
		guild::Guild,
		id::GuildId,
		channel::Message,
		gateway::Ready
	},
	framework::standard::{
		StandardFramework,
		CommandResult,
		macros::{
			command,
			group
		}
	}
};

use rusqlite::{
	Connection,
	params
};

use rand::{
	thread_rng,
	Rng
};

#[macro_use]
extern crate lazy_static;

fn print_help() {
	println!("Commands:");
	println!("\th, help\n\t\tShows this help message");
	println!("\tq, quit\n\t\tExits the prompt and stops the bot. This is the suggested way to stop to bot");
	println!("\tf, find\n\t\tQueries the database for a word");
}

#[group]
#[commands(wordcount, help, compliment, ssif, ticket, walter)]
struct General;

struct Handler;

fn sanitize_string(s: &str) -> String {
	let mut s_new: String = String::new();
	if !(s.starts_with("http://") || s.starts_with("https://")) {
		for (_size, c) in s.char_indices() {
			if (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') {
				s_new.push(c);
			}
		}
	}
	s_new
}

#[async_trait]
impl EventHandler for Handler {
	async fn message(&self, ctx: Context, msg: Message) {
		if *MY_ID.lock().unwrap() != *msg.author.id.as_u64() && !(msg.author.bot) && !msg.is_private() {
			for word in msg.content.split_whitespace() {
				let word = &sanitize_string(word.trim());
				if !word.is_empty() {
					insert_word(&DB.lock().unwrap(), Word{
						server: *msg.guild_id.unwrap().as_u64() as i64,
						word: String::from(word),
						uses: 1,
					});
				}
				if (word == "dOG") && (thread_rng().gen_range(0,3) == 0) {
					msg.channel_id.say(ctx.clone(),"dOG").await.unwrap();
				}
			}
		}
	}

async fn ready(&self, _ctx: Context, ready: Ready) {
		println!("[ INFO ] Successfully connected to {}!", ready.user.name);
		_ctx.set_presence(
				Some(serenity::model::gateway::Activity::playing("Feed me word, :help")),
				serenity::model::user::OnlineStatus::Online
		).await;
		*MY_ID.lock().unwrap() = *ready.user.id.as_u64();
	}
}

#[derive(Clone)]
struct Word {
	server: i64,
	word: String,
	uses: i32
}

impl Word {
	fn new() -> Word {
		Word{server: -1, word: "invalid".to_string(), uses: 0}
	}
}

lazy_static! {
	#[allow(non_upper_case_globals)]
	static ref DB: Arc<Mutex<Connection>> = {
		let _db: Connection;
		if !Path::new("./test.sqlite").exists() {
			_db = Connection::open("test.sqlite").unwrap();
			_db.execute("CREATE TABLE words (server INTEGER, word TEXT, uses INTEGER);",params![])
				.expect("[ ERROR ] Could not execute databse command");
			println!("[ INFO ] Created database and added word table");
		} else {
			_db = Connection::open("test.sqlite").unwrap();
			println!("[ INFO ] Database already exists");
		}
		println!("[ INFO ] Connected to the database");
		Arc::new(Mutex::new(_db))
	};

	static ref MY_ID: Arc<Mutex<u64>> = {
		Arc::new(Mutex::new(1))
	};
}

fn insert_word(database: &rusqlite::Connection, word: Word) {
	let _db = Box::new(database);
	let command_one = format!(
		"INSERT INTO words (server, word, uses)
		SELECT {a}, \"{b}\", 0
		WHERE NOT EXISTS (
			SELECT 1 FROM words WHERE server = {a} AND word = \"{b}\"
		);" ,a = word.server, b = word.word
	);
	let command_two = format!(
		"UPDATE words SET uses = uses + 1 WHERE server = {a} AND word = \"{b}\"",
		a = word.server, b = word.word
	);
	_db.execute(
		&command_one,
		params![]
	).unwrap();
	_db.execute(
		&command_two,
		params![]
	).unwrap();
}

fn query_word(database: &rusqlite::Connection, query: String, guild_id: u64) -> i32 {
	let _db = Box::new(database);
	let mut statement;
	match _db.prepare(&format!("SELECT * FROM words WHERE word = \"{}\" AND \"{}\";", query, guild_id)) {
		Ok(n) => statement = n,
		Err(_e) => {
			println!("\"{}\" : Not in database", String::from(&query));
			return -1;
		}
	}
	let mut word_iter = statement.query_map(params![], |row| {
		Ok(Word {
			server: row.get(0)?,
			word: String::from(&query),
			uses: row.get(2)?,
		})
	}).unwrap();
	let word: Word;
	match word_iter.next() {
		None => return -1,
		Some(v) => {
			match v {
				Ok(n) => word = n,
				Err(_e) => word = Word::new()
			}
		}
	};
	word.uses
}

fn query_words(database: &rusqlite::Connection, query: String) -> Vec<Word> {
	let mut words: Vec<Word> = Vec::new();
	let _db = Box::new(database);
	let mut statement;
	match _db.prepare(&format!("SELECT * FROM words WHERE word = \"{}\";",query)) {
		Ok(n) => statement = n,
		Err(_e) => {
			println!("\"{}\" : Not in database", String::from(&query));
			return words;
		}
	}
	let word_iter = statement.query_map(params![], |row| {
		Ok(Word{
			server: row.get(0)?,
			word: String::from(&query),
			uses: row.get(2)?,
		})
	}).unwrap();
	for word in word_iter {
		match word {
			Ok(n) => words.push(n),
			Err(e) => println!("[ ERROR ] Word not used anywhere\n{}",e)
		}
	}
	words
}

#[allow(non_snake_case)]
mod SSIF;

#[allow(non_upper_case_globals)]
#[allow(non_snake_case)]
mod Compliment;

#[tokio::main]
async fn main() -> Result<(), std::io::Error> {
	let mut args: Vec<String> = std::env::args().collect();
	let mut prefix = ":";
	args.remove(0);
	for arg in args {
		println!("{}", arg);
		match &arg[..] {
			"-t" | "--test" => {
				println!("Entered testing mode");
				prefix = ">:";
			}
			_ => {
				return Err(std::io::Error::new(
					ErrorKind::InvalidInput,
					format!("Error unrecognised argument \"{}\"", arg)
				));
			}
		}
	}
	Compliment::load_compliments("compliments");

	let token = fs::read_to_string("../wordbot.token")
		.expect("[ ERROR ] Could not read token file");
	println!("{}", token);
	//validate_token(&token)
	//	.expect("[ ERROR ] Invalid token");

	/*/
	let mut client = Client::new(
		token,
		Handler
	).expect("[ERROR] Could not create client");
	 */
	let mut client = Client::builder(token)
		.event_handler(Handler)
		.framework(
			StandardFramework::new().configure(
				|c| c.prefix(prefix)
			).group(&GENERAL_GROUP)
		).await
		.expect("[ERROR] Could not create client");
	client.start().await.expect("[ERROR] Could not start client");
	SSIF::init();
	Ticket::init(&DB.lock().unwrap());

	let http_method = &(client.cache_and_http).http.clone();

	/*/client.with_framework(StandardFramework::new()
		.configure(|c| c.prefix(prefix))
		.group(&GENERAL_GROUP)
	);*/

	let _client_thread = thread::spawn(async move || {
		if let Err(why) = client.start().await {
			println!("[ERROR] An error ocurred while connecting to the client:\n{:?}",why);
		}
	});

	println!("[INFO] Started interpreter");
	let stdin = stdin();
	//let mut handle = stdin.lock();

	async fn get_guild_name(http: &Arc<serenity::http::Http>, server_id: u64) -> Option<String> {
		match Guild::get(
		    http,
		    GuildId(server_id)
		).await {
		    Ok(n) => Some(n.name),
		    Err(_e) => None
		}
	}

	loop {
		print!("> ");
		stdout().flush().unwrap();
		let mut buffer = String::new();
		stdin.read_line(&mut buffer)
			.expect("[ ERROR ] Could not read from stdin");
		buffer = buffer.trim_end().to_string();
		match buffer.as_str() {
			"h" | "help" => print_help(),
			"q" | "quit" => {
				//TODO: Implement bot shutdown
				return Ok(());
			},
			"f" | "find" => {
				print!("Enter a query string:\n>> ");
				stdout().flush().unwrap();
				buffer = String::new();
				stdin.read_line(&mut buffer).unwrap();
				buffer = buffer.trim_end().to_string();
				let mut uses = query_words(&DB.lock().unwrap(), buffer.clone());
				#[allow(non_upper_case_globals)]
				if !uses.is_empty() {
					if uses.len() == 1 {
						let word = uses.pop().unwrap();
						let guild_name = get_guild_name(
							http_method,
							word.server as u64
						);
						match guild_name.await {
							Some(name) =>
								println!(
									"{} uses in {}",
									word.uses,
									name
								),
							None => println!("[ERROR] Could not connect to server {}", word.server)
						}
					} else {
						let mut i: u32 = 0;
						#[allow(clippy::explicit_counter_loop)]
						let mut selection: Vec<(Word,String)> = Vec::new();
						#[allow(clippy::explicit_counter_loop)]
						for use_ in uses {
							let name = get_guild_name(http_method, use_.server as u64);
							match name.await {
								Some(n) => {
									selection.push((use_.clone(),n.clone()));
									println!("{}.) {}",i, n);
									i+=1;
								},
								None => println!("[INFO] Server not available")
							}
						}
						let choice = || -> &(Word, String) {
							loop {
								print!(">> ");
								stdout().flush().unwrap();
								buffer = String::new();
								stdin.read_line(&mut buffer).unwrap();
								buffer = String::from(buffer.trim());
								match buffer.parse::<u32>() {
									Ok(a) => {
										return selection.get(a as usize).unwrap();
									},
									Err(_e) => {
										println!("[ERROR] Not a valid int");
										continue;
									}
								}
							}
						}();
						println!("\"{}\" has {} uses in {}",
							choice.0.word,
							choice.0.uses,
							choice.1
						);
					}
				} else {
					println!("\"{}\" has not been used yet", buffer);
				}
			}
			"d" | "delete" => {
				//TODO: implement a method to delete entries
			}
			_ => {
				println!("Unknown command: \"{}\"", buffer);
				print_help();
			}
		}
	}
}

#[command]
async fn wordcount(ctx: &Context, msg: &Message) -> CommandResult {
	if msg.is_private() {
		msg.channel_id.say(&ctx, "Cannot use `:wordcount` not in a server").await.unwrap();
		return Ok(())
	}
	println!("{}",msg.content);
	let word = msg.content.split_whitespace().nth(1).unwrap();
	let uses: i32 = query_word(
		&DB.lock().unwrap(),
		String::from(msg.content.split_whitespace().nth(1).unwrap()),
		*msg.guild_id.unwrap().as_u64()
	);
	if uses <= 0 {
		msg.channel_id.say(ctx, format!("\"{}\" has not been used yet",word)).await.unwrap();
	} else {
		msg.channel_id.say(ctx,format!("\"{}\" has been used {} times",
			word,
			uses
		)).await?;
	}
	Ok(())
}

mod help;

#[command]
async fn help(ctx: &Context, msg: &Message) -> CommandResult {
	//msg.channel_id.say(ctx,"`:wordcount <Word>` Tells you how many times a word has been used in the current server").unwrap();
	match help::get_page(|| -> String {
		match msg.content.split_whitespace().nth(1) {
			Some(v) => String::from(v),
			None => String::from("topics")
		}
	}()) {
		Some(v) => msg.channel_id.say(ctx, v).await.unwrap(),
		None => msg.channel_id.say(ctx, help::get_page(String::from("topic")).unwrap()).await.unwrap()
	};
	Ok(())
}

#[command]
async fn compliment(ctx: &Context, msg: &Message) -> CommandResult {
	//msg.channel_id.say(ctx,format!("content: {}",msg.content)).unwrap();
	let author = &msg.author;
	if msg.mentions.is_empty() {
		msg.channel_id.say(ctx,Compliment::get_compliment(&author)).await.unwrap();
	} else {
		msg.channel_id.say(ctx,Compliment::get_compliment(&msg.mentions[0])).await.unwrap();
	}
	Ok(())
}

#[command]
async fn ssif(ctx: &Context, msg: &Message) -> CommandResult {
	match msg.content.split_whitespace().nth(1) {
		None => {
			msg.channel_id.say(ctx,"No language passed").await.unwrap();
		}
		Some(n) => {
			match SSIF::ssif(String::from(n)) {
				Ok(v) => {
					msg.channel_id.say(ctx, &v).await.unwrap();
				}
				Err(e) => {
					msg.channel_id.say(ctx, e).await.unwrap();
				}
			}
		}
	};
	Ok(())
}
		}
	};
	Ok(())
}
